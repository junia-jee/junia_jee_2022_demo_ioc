package junia.demo.ioc.apps.javaversion.config;

import junia.demo.ioc.domain.drawing.Drawing;
import junia.demo.ioc.domain.shape.Circle;
import junia.demo.ioc.domain.shape.Rectangle;
import junia.demo.ioc.domain.shape.Shape;
import junia.demo.ioc.domain.shape.Square;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

@Configuration
@ComponentScan(basePackages = "junia.demo.ioc.domain",
        includeFilters = {
                @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = Drawing.class),
        })
public class ApplicationConfig {

    @Bean
    public Shape circle() {
        return new Circle(4);
    }

    @Bean
    public Shape square() {
        return new Square(2);
    }

    @Bean
    public Shape rectangle() {
        return new Rectangle(1, 3);
    }
}
