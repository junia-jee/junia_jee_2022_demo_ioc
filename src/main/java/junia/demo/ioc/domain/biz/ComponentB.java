package junia.demo.ioc.domain.biz;

import junia.demo.ioc.domain.ddd.BusinessService;

@BusinessService
public class ComponentB {

    private final ComponentC componentC;
    private final ComponentD componentD;


    public ComponentB(ComponentC componentC, ComponentD componentD) {
        this.componentC = componentC;
        this.componentD = componentD;
    }

    public ComponentC getComponentC() {
        return componentC;
    }

    public ComponentD getComponentD() {
        return componentD;
    }
}
