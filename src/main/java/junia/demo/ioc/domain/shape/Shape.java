package junia.demo.ioc.domain.shape;

public interface Shape {

    double getPerimeter();

    double getSurface();
}
