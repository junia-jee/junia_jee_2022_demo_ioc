package junia.demo.ioc.domain.shape;

public class Square implements Shape {

    private final double side;


    public Square(double side) {
        this.side = side;
    }


    @Override
    public double getPerimeter() {
        return side * 4;
    }


    @Override
    public double getSurface() {
        return side * side;
    }


}
